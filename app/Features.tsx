import React from 'react'
import { 
  IdentificationIcon, DocumentMagnifyingGlassIcon, GlobeAltIcon,TableCellsIcon, ScaleIcon, PencilSquareIcon, BoltIcon
} from '@heroicons/react/24/outline'

const features = [
    {
      name: 'Literature review',
      description:
        'IROC includes tools for comparing different datasets or studies, allowing researchers to see patterns and trends more easily.',
      icon: DocumentMagnifyingGlassIcon,
    },
    {
      name: 'Notebook',
      description:
        "Streamline your research with IROC's notebook tool.\n Let us manage note grading, labbook transfer, and literature verification so you can focus on what matters most. With IROC, you have access to all the tools you need to succeed in your research.",
      icon: BoltIcon,
    },
    {
      name: 'Marketplace',
      description: `IROC includes tools for taking notes and organizing information, helping researchers stay organized and on top of their work.
      Automated billing and purchasing: IROC can help researchers automate the process of requesting and purchasing materials or equipment needed for their research.`,
      icon: PencilSquareIcon,
    },
    {
      name: 'Exchange',
      description: `IROC can help researchers find and keep track of relevant conferences, journals, grants, and job opportunities in their field.`,
      icon: GlobeAltIcon,
    },
  ]

export default function Features() {
  return (
    <div className="bg-white py-24 sm:py-32 lg:py-40">
      <div className="mx-auto max-w-7xl px-6 lg:px-8">
        <div className="sm:text-center">
          <h2 className="text-lg font-semibold leading-8 text-yellow-600">Applications</h2>
          <p className="mt-2 text-3xl font-bold tracking-tight text-gray-900 sm:text-4xl">Save time and focus on the important information with IROC</p>
          <p className="mx-auto mt-6 max-w-2xl text-lg leading-8 text-gray-600">
           IROC helps you clarify and understand the data you're working with, making it easier to draw meaningful conclusions and move your research forward.
          </p>
        </div>

        <div className="mt-20 max-w-lg sm:mx-auto md:max-w-none">
          <div className="grid grid-cols-1 gap-y-16 md:grid-cols-2 md:gap-x-12 md:gap-y-16">
            {features.map((feature) => (
              <div key={feature.name} className="relative flex flex-col gap-6 sm:flex-row md:flex-col lg:flex-row">
                <div className="flex h-12 w-12 items-center justify-center rounded-xl bg-yellow-500 text-white sm:shrink-0">
                  <feature.icon className="h-8 w-8" aria-hidden="true" />
                </div>
                <div className="sm:min-w-0 sm:flex-1">
                  <p className="text-lg font-semibold leading-8 text-gray-900">{feature.name}</p>
                  <p className="mt-2 text-base leading-7 text-gray-600">{feature.description}</p>
                </div>
              </div>
            ))}
          </div>
        </div>
      </div>
    </div>
  )
}
