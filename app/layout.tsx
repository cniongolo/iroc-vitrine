import '../styles/globals.css';

export default function RootLayout({
  children,
}: {
  children: React.ReactNode
}) {
  return (
    <html>
      <head>
        <link rel="stylesheet" href="" />
      </head>
      <body className="font-inter antialiased bg-white text-gray-900 tracking-tight vsc-initialized" data-aos-easing="ease-out-cubic" data-aos-duration="700" data-aos-delay="0">
        {children}
        </body>
    </html>
  )
}
