import React from 'react';
import { BuildingStorefrontIcon, GlobeAltIcon, PencilSquareIcon, 
    DocumentMagnifyingGlassIcon, ArrowPathRoundedSquareIcon, RectangleGroupIcon, 
    NewspaperIcon, BeakerIcon, UserGroupIcon, ArrowsRightLeftIcon,
    BriefcaseIcon, BanknotesIcon, BuildingLibraryIcon, MagnifyingGlassIcon, MagnifyingGlassCircleIcon,
    ReceiptPercentIcon, DocumentTextIcon , LinkIcon

} from '@heroicons/react/24/outline';


const ApplicationLeft = ({ application, index} : any) => {
    return (
        <React.Fragment>
            <div className={`flex flex-col items-center space-y-8 lg:flex-row lg:space-y-0 items-center lg:justify-between lg:space-x-16`}>
                            <div className="flex w-full flex-col space-y-8 lg:w-6/12">
                                <div className="w-full flex-col space-y-4">
                                    <div className="flex flex-col justify-center space-y-4 px-1.5">
                                        <div className="flex h-14 w-14 items-center justify-center rounded-xl bg-yellow-500 text-white">
                                            {application.icon && <application.icon className="h-8 text-white-500" />}
                                        </div>
                                        <div>
                                            <h2 className="text-2xl font-bold"><span className="text-3xl text-dark">{application.title}</span></h2>
                                        </div>
                                    </div>
                                    <p className="px-2 text-gray-600 dark:text-gray-600">{application.description}</p>
                                    <p className="px-2 text-gray-600 dark:text-gray-600"></p>
                                </div>
                                <div className="grid w-full grid-cols-1 gap-6 md:grid-cols-2">
                                    {
                                        application.infos.map((info : any, index: any) => {
                                            return (
                                                <div className="flex-auto rounded-2xl border border-black-300 p-6 dark:bg-black-500">
                                                    <h3 className="flex flex-col space-y-3 text-lg font-semibold xl:text-xl dark:text-dark">
                                                        <span>
                                                            {info.icon && <info.icon className="h-8 text-yellow-500" />}
                                                        </span>
                                                        <span>{info.title}</span>
                                                    </h3>
                                                    <div className="py-2.5">
                                                        <div className="flex flex-col space-y-3 text-sm dark:text-gray-600">
                                                        <p>{info.description}</p>
                                                        </div>
                                                    </div>
                                                </div>
                                            )
                                        })
                                    }
                                </div>
                            </div>
                            <div className="flex w-full items-center lg:w-6/12">
                            <div>
                                <div className="vsc-controller" />
                                    <img src={application.image} width="100%" height="auto" alt="" />
                                </div>
                            </div>
                        </div>
        </React.Fragment>
    );
};

const ApplicationRight = ({ application, index} : any) => {
    return (
        <React.Fragment>
            <div className={`flex flex-col items-center space-y-8 lg:flex-row lg:space-y-0 items-center lg:justify-between lg:space-x-16 flex-col-reverse lg:flex-row`}>
                <div className="w-full lg:w-6/12 rounded-xl my-6">
                    <div>
                        <div className="vsc-controller" />
                            <img src={application.image} width="100%" height="auto" alt="" />
                        </div>
                </div>
                <div className="flex w-full flex-col justify-between lg:w-6/12 space-y-4 md:space-y-8">
                    <div className="w-full flex-col space-y-4">
                        <div className="flex flex-col justify-center space-y-4 px-1.5">
                            <div className="flex h-14 w-14 items-center justify-center rounded-xl bg-yellow-500 text-white">
                                            {application.icon && <application.icon className="h-8 text-white-500" />}
                            </div>
                                        <div>
                                            <h2 className="text-2xl font-bold"><span className="text-3xl text-dark">{application.title}</span></h2>
                                        </div>
                                    </div>
                                    <p className="px-2 text-gray-600 dark:text-gray-600">{application.description}</p>
                                    <p className="px-2 text-gray-600 dark:text-gray-600"></p>
                                </div>
                                <div className="grid w-full grid-cols-1 gap-6 md:grid-cols-2">
                                    {
                                        application.infos.map((info: any, index : any) => {
                                            return (
                                                <div className="flex-auto rounded-2xl border border-black-300 p-6 dark:bg-black-500">
                                                    <h3 className="flex flex-col space-y-3 text-lg font-semibold xl:text-xl dark:text-dark">
                                                        <span>
                                                            {info.icon && <info.icon className="h-8 text-yellow-500" />}
                                                        </span>
                                                        <span>{info.title}</span>
                                                    </h3>
                                                    <div className="py-2.5">
                                                        <div className="flex flex-col space-y-3 text-sm dark:text-gray-600">
                                                        <p>{info.description}</p>
                                                        </div>
                                                    </div>
                                                </div>
                                            )
                                        })
                                    }
                                </div>
                </div>
            </div>
        </React.Fragment>
    );
}

const Applications : React.FC  = () => {
    const applications = [
         {
            title: 'IROC Literature review',
            description: `
                Optimize your data in less time with IROC's literature review tool. Let us handle the data reading, sorting, comparing, and analysis so you can focus on the important tasks. 
                IROC gives you the tools you need to succeed in your research.
            `,
            icon: DocumentMagnifyingGlassIcon,
            direction: 'left',
            image: 'https://gitlab.com/cniongolo/iroc-vitrine/-/raw/main/public/images/screens/literature.png',
            infos : [
                {
                    title: 'NewIn',
                    description: `
                        Newin feature keeps you on top of the latest developments in your field by centralizing new publications in one convenient location. 
                    `,
                    icon: NewspaperIcon
                },
                {
                    title: 'Recap',
                    description: `
                        Eliminate repetitions and streamline your research with IROC's Recap feature. 
                        Our mission is to help you focus on the important information and advance your research efficiently
                    `,
                    icon: DocumentTextIcon
                },
                {
                    title: 'Comparison',
                    description: `
                        Advance your research with IROC's Comparison feature. 
                        Easily compare publications on the same topic from different domains and see patterns and trends in different data sets and studies.
                    `,
                    icon: ArrowsRightLeftIcon
                },
                {
                    title: 'New findings',
                    description: `
                        Discover unpublished connections and stay ahead of the curve in your field with IROC's New Findings feature. 
                        Find new ideas and gain a competitive edge in your research with IROC.
                    `,
                    icon: LinkIcon
                }
            ]

        },
        {
            title: 'IROC Notebook',
            description: `
                IROC's notebook tool is the ultimate research companion, helping you stay organized and on top of your work with features like notes classification, labbook transfer, and literature checkups.
                
                With IROC, you can easily manage your research and stay focused on the important tasks at hand
            `,
            icon: PencilSquareIcon,
            direction: 'rigth',
            image: 'https://gitlab.com/cniongolo/iroc-vitrine/-/raw/main/public/images/screens/notebooks.png',
            infos : [
                {
                    title: 'Search',
                    description: `
                        Search feature makes it easy to find your notes quickly and easily, even years later. With IROC, you'll never lose track of important information or waste time searching for what you need. 
                        Stay organized and on top of your work with IROC's Search feature.
                    `,
                    icon: MagnifyingGlassIcon
                },
                {
                    title: 'Remodel my Note (from experiments)',
                    description: `
                        This feature will helps you reformat your notes from experiments into original, transmissible sentences that can be easily incorporated into your lab book. 
                        With IROC, you can save time and effort while staying organized and on top of your work.
                    `,
                    icon: RectangleGroupIcon
                },
                {
                    title: 'Similarity check', 
                    description: `
                        The similarities check feature makes it easy to check the results and notes of your experiments against the literature, ensuring the accuracy and validity of your research. 
                        With IROC, you can confidently move your research forward with the knowledge that your findings are well-supported.
                    `,
                    icon: ArrowPathRoundedSquareIcon
                }
            ]
        },
        {
            title: 'IROC Exchange',
            description: `
                The Exchange tool helps you stay connected with the research community and keep up with the latest opportunities in your field. 
                Connect, collaborate, and advance your research by accessing conferences, jobs, journals, and grants, saving you time and effort.
            `,
            icon: GlobeAltIcon,
            direction: 'left',
            image: 'https://gitlab.com/cniongolo/iroc-vitrine/-/raw/main/public/images/screens/exchange-clean.png',
            infos : [
                {
                    title: 'Congresses',
                    description: `
                        The IROC Congress helps you keep up with the latest developments in your field and connect with other researchers at conferences and congresses. 
                        Find relevant events and stay informed about what's happening in your field.
                    `,
                    icon: UserGroupIcon
                },
                {
                    title: 'Jobs',
                    description: `
                        IROC's Jobs feature helps you find and apply for the latest research opportunities in your field. 
                        Search and find relevant job openings and track your job search in one place.
                    `,
                    icon: BriefcaseIcon
                },
                {
                    title: 'Journals',
                    description: `
                        The Journals feature allows you to stay informed about the latest research and developments in your field. 
                        With IROC, you can easily search and find relevant journals and access the latest publications in your field.
                    `,
                    icon: NewspaperIcon
                },
                {
                    title: 'Grants',
                    description: `
                        IROC's Grants helps you find and apply for funding opportunities for your research. 
                        You can easily search and find relevant grants and streamline the application process, giving you the support you need to advance your research.
                    `,
                    icon: BuildingLibraryIcon
                },
                
            ]

        },
        {
            title: 'IROC Marketplace',
            description: `
                IROC's Marketplace feature helps you streamline the process of acquiring the materials and equipment you need for your research. 
                With IROC, you can easily search and index products and automate billing requests, saving you time and effort. 
                Make your research more efficient with our Marketplace.
            `,
            icon: BuildingStorefrontIcon,
            direction: 'right',
            image: 'https://gitlab.com/cniongolo/iroc-vitrine/-/raw/main/public/images/screens/marketplace.png',
            infos : [
                {
                    title: 'Search',
                    description: `
                        IROC makes it easy to find the products you need from a variety of websites, saving you time and money. 
                        With IROC, you can quickly and easily index and compare products to find the one that best suits your search.
                    `,
                    icon: MagnifyingGlassCircleIcon
                },
                {
                    title: 'Ask for quotation',
                    description: `
                        Simplifies the billing process for your research acquisitions. 
                        With IROC, you can easily request quotes and track your purchases, streamlining the process and saving you time and effort.
                    `,
                    icon: ReceiptPercentIcon
                }, {
                    title: 'Sell',
                    description: `
                        You have the opportunity to present and sell your own products to a community of researchers. 
                        With IROC, you can easily connect with potential buyers and make your research more profitable.
                    `,
                    icon: BanknotesIcon
                }
            ]

        },
    ]
    return (
        <React.Fragment>
            {
                applications.map((application, index) => {
                    if (application.direction === 'left') {
                       return <ApplicationLeft application={application} index={index} />
                    }
                    return (
                        <ApplicationRight application={application} index={index} />
                    )
                })
            }
        </React.Fragment>
    )
}

export default function IROC() {
  return (
    <React.Fragment>
        <hr className="w-full border border-dark-50 dark:border-black-400" />
        <div className="bg-white py-24 sm:py-32 lg:py-40" id="product">
            <div className="mx-auto max-w-7xl px-6 lg:px-8">
                <div className="sm:text-center">
                    <h2 className="text-lg font-semibold leading-8 text-yellow-600">Applications</h2>
                    <p className="mt-2 text-3xl font-bold tracking-tight text-gray-900 sm:text-4xl">The smart way to conduct research, with Literature Review, Notebook, Marketplace, and Exchange features at your fingertips</p>
                    <p className="mx-auto mt-6 max-w-2xl text-lg leading-8 text-gray-600">
                        IROC's literature review, notebook, marketplace, and exchange features provide a comprehensive platform for tracking and recording your research progress, accessing a vast collection of scientific publications, collaborating and exchanging information with other researchers, and connecting with a community of scientists and researchers looking to buy, sell, or trade materials and equipment, making it the ultimate research tool for scientists
                    </p>
                </div>
                <br />
                <br />
                <div className='flex flex-col space-y-24 md:space-y-36'>
                    <Applications />
                </div>
            </div>
        </div>
    </React.Fragment>
  );
}
