import React from 'react'

export default function Logos() {
    return (
      <React.Fragment>
        <section className="relative pt-28 pb-36 bg-blueGray-100 overflow-hidden">
          <div className="mx-auto mb-[60px] max-w-[620px] text-center lg:mb-20 sm:text-center">
            <h2 className="text-lg font-semibold leading-8 text-yellow-600">Board</h2>
          </div>
          <div className="relative z-10 container px-4 mx-auto">
              <div className="flex flex-wrap max-w-5xl mx-auto -m-3">
                <div className="w-full md:w-1/2 lg:w-1/4 p-3">
                    <div className="flex items-center justify-center py-8 px-9 h-full bg-white rounded-3xl">
                      <img src="https://kclsed.org/wp-content/uploads/2020/02/yale-university-logo-png-2.png" alt="isefre" />
                    </div>
                </div>
              </div>
          </div>
        </section>
      </React.Fragment>
    )
  }
