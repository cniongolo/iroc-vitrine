import React from 'react';
import { 
    CheckCircleIcon
} from '@heroicons/react/24/outline';



export default function Pricing() {
    const pricing = [ 
        {
            category: "classic",
            description: "Your license is valid for all the features of IROC.",
            price: 30,
            features : [
                {
                    feature: "Optimized workload & cost reduction",
                },
                {
                    feature: "Animal survival rate increase",
                },
                {
                    feature: "Promotion of international collaboration"
                },
                {
                    feature: "Share and extension of knowledge"
                },
                {
                    feature: "Higher ranked and qualitative publications"
                },
                {
                    feature: "Increase of patents"
                },
                {
                    feature: "Increase of patents"
                }
            ]
        }
    ]
  return (
   <React.Fragment>
    <section className="bg-white py-24 sm:py-32 lg:py-40 yellowscale">
        <div className="mx-auto max-w-7xl px-6 lg:px-8">
            <div className="-mx-4 flex flex-wrap">
                <div className="w-full px-4">
                    <div className="mx-auto mb-[60px] max-w-[620px] text-center lg:mb-20">
                        <span className="text-lg font-semibold leading-8 text-yellow-600">Pricing</span>
                        <h2 className="mb-4 text-3xl font-bold text-dark sm:text-4xl md:text-[42px]">
                            Our Pricing Plan
                        </h2>
                        <p className='text-lg leading-relaxed text-body-color sm:text-xl sm:leading-relaxed'>
                        IROC prices indexed on server prices to allow availability for all institutions.
                        </p>
                    </div>
                    <div className='flex flex-col items-start items-center space-y-6 lg:space-y-0 justify-center lg:flex-row lg:space-x-8'>
                        <div className="
                            flex w-full flex-col justify-between space-y-4 rounded-2xl bg-black border border-gray-200
                            p-6 shadow-2xl shadow-transparent px-8 shadow-lg  duration-500 dark:border-black-300 dark:bg-black-500 lg:w-4/12 
                            lg:p-5 xl:p-6 2xl:w-3/12 
                            ">
                            <div className="flex flex-col space-y-1.5">
                                <h4 className="text-lg font-semibold">
                                    <span className="font-bold text-white">Classic</span>
                                </h4>
                                <span className="text-lg text-gray-500  dark:text-gray-400">Your license is valid for all the kits, forever.</span>
                            </div>
                            <div className="flex items-center space-x-3">
                                <div><span className="text-2xl text-white font-extrabold dark:text-white lg:text-3xl">30€/mo </span></div>
                            </div>
                            <div className="my-2 py-2">
                                <ul className="flex flex-col space-y-4">
                                <li className="flex items-center space-x-4 font-medium">
                                        <div>
                                        <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" strokeWidth="1.5" stroke="currentColor" aria-hidden="true" className="h-6 text-green-500">
                                            <path strokeLinecap="round" strokeLinejoin="round" d="M9 12.75L11.25 15 15 9.75M21 12a9 9 0 11-18 0 9 9 0 0118 0z" />
                                        </svg>
                                        </div>
                                        <span className="text-base text-gray-500 dark:text-white">Partnerships creation</span>
                                    </li>
                                    <li className="flex items-center space-x-4 font-medium">
                                        <div>
                                        <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" strokeWidth="1.5" stroke="currentColor" aria-hidden="true" className="h-6 text-green-500">
                                            <path strokeLinecap="round" strokeLinejoin="round" d="M9 12.75L11.25 15 15 9.75M21 12a9 9 0 11-18 0 9 9 0 0118 0z" />
                                        </svg>
                                        </div>
                                        <span className="text-base text-gray-500 dark:text-white">Optimized workload & cost reduction</span>
                                    </li>
                                    <li className="flex items-center space-x-4 font-medium">
                                        <div>
                                        <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" strokeWidth="1.5" stroke="currentColor" aria-hidden="true" className="h-6 text-green-500">
                                            <path strokeLinecap="round" strokeLinejoin="round" d="M9 12.75L11.25 15 15 9.75M21 12a9 9 0 11-18 0 9 9 0 0118 0z" />
                                        </svg>
                                        </div>
                                        <span className="text-base text-gray-500 dark:text-white">Promotion of international collaboration</span>
                                    </li>
                                    <li className="flex items-center space-x-4 font-medium">
                                        <div>
                                        <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" strokeWidth="1.5" stroke="currentColor" aria-hidden="true" className="h-6 text-green-500">
                                            <path strokeLinecap="round" strokeLinejoin="round" d="M9 12.75L11.25 15 15 9.75M21 12a9 9 0 11-18 0 9 9 0 0118 0z" />
                                        </svg>
                                        </div>
                                        <span className="text-base text-gray-500 dark:text-white">Share and extension of knowledge</span>
                                    </li>
                                    <li className="flex items-center space-x-4 font-medium">
                                        <div>
                                        <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" strokeWidth="1.5" stroke="currentColor" aria-hidden="true" className="h-6 text-green-500">
                                            <path strokeLinecap="round" strokeLinejoin="round" d="M9 12.75L11.25 15 15 9.75M21 12a9 9 0 11-18 0 9 9 0 0118 0z" />
                                        </svg>
                                        </div>
                                        <span className="text-base text-gray-500 dark:text-white">Higher ranked and qualitative publications</span>
                                    </li>
                                    <li className="flex items-center space-x-4 font-medium">
                                        <div>
                                        <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" strokeWidth="1.5" stroke="currentColor" aria-hidden="true" className="h-6 text-green-500">
                                            <path strokeLinecap="round" strokeLinejoin="round" d="M9 12.75L11.25 15 15 9.75M21 12a9 9 0 11-18 0 9 9 0 0118 0z" />
                                        </svg>
                                        </div>
                                        <span className="text-base text-gray-500 dark:text-white">Increase of patents</span>
                                    </li>
                                </ul>
                            </div>
                            <div className="relative w-full md:w-auto" data-headlessui-state>
                                <span className="Button bg-yellow-500    cursor-pointer rounded-full" aria-expanded="false" data-headlessui-state id="headlessui-popover-button-:rm:">
                                    <span className="ButtonLarge flex items-center space-x-4">
                                        <span className="text-sm font-bold text-white">Subscribe</span>
                                            {/* <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor" aria-hidden="true" className="h-5 w-5 transition duration-150 ease-in-out group-hover:text-opacity-80">
                                                <path fillRule="evenodd" d="M5.23 7.21a.75.75 0 011.06.02L10 11.168l3.71-3.938a.75.75 0 111.08 1.04l-4.25 4.5a.75.75 0 01-1.08 0l-4.25-4.5a.75.75 0 01.02-1.06z" clipRule="evenodd" />
                                            </svg> */}
                                    </span>
                                </span>
                            </div>
                        </div>

                        <div className="
                            flex w-full flex-col justify-between space-y-4 rounded-2xl bg-black border border-gray-200
                            p-6 shadow-2xl shadow-transparent px-8 shadow-lg  duration-500 dark:border-black-300 dark:bg-black-500 lg:w-4/12 
                            lg:p-5 xl:p-6 2xl:w-3/12 
                            ">
                            <div className="flex flex-col space-y-1.5">
                                <h4 className="text-lg font-semibold">
                                    <span className="font-bold text-white">Advanced A.I</span>
                                </h4>
                                <span className="text-lg text-gray-500  dark:text-gray-400">Your license is valid for all the kits, forever.</span>
                            </div>
                            <div className="flex items-center space-x-3">
                                <div><span className="text-2xl text-white font-extrabold dark:text-white lg:text-3xl">50€/mo</span></div>
                            </div>
                            <div className="my-2 py-2">
                                <ul className="flex flex-col space-y-4">
                                    <li className="flex items-center space-x-4 font-medium">
                                        <div>
                                        <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" strokeWidth="1.5" stroke="currentColor" aria-hidden="true" className="h-6 text-green-500">
                                            <path strokeLinecap="round" strokeLinejoin="round" d="M9 12.75L11.25 15 15 9.75M21 12a9 9 0 11-18 0 9 9 0 0118 0z" />
                                        </svg>
                                        </div>
                                        <span className="text-base text-gray-500 dark:text-white">Use the power of AI and GPT </span>
                                    </li>
                                    <li className="flex items-center space-x-4 font-medium">
                                        <div>
                                        <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" strokeWidth="1.5" stroke="currentColor" aria-hidden="true" className="h-6 text-green-500">
                                            <path strokeLinecap="round" strokeLinejoin="round" d="M9 12.75L11.25 15 15 9.75M21 12a9 9 0 11-18 0 9 9 0 0118 0z" />
                                        </svg>
                                        </div>
                                        <span className="text-base text-gray-500 dark:text-white">Optimized workload & cost reduction</span>
                                    </li>
                                    <li className="flex items-center space-x-4 font-medium">
                                        <div>
                                        <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" strokeWidth="1.5" stroke="currentColor" aria-hidden="true" className="h-6 text-green-500">
                                            <path strokeLinecap="round" strokeLinejoin="round" d="M9 12.75L11.25 15 15 9.75M21 12a9 9 0 11-18 0 9 9 0 0118 0z" />
                                        </svg>
                                        </div>
                                        <span className="text-base text-gray-500 dark:text-white">Promotion of international collaboration</span>
                                    </li>
                                    <li className="flex items-center space-x-4 font-medium">
                                        <div>
                                        <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" strokeWidth="1.5" stroke="currentColor" aria-hidden="true" className="h-6 text-green-500">
                                            <path strokeLinecap="round" strokeLinejoin="round" d="M9 12.75L11.25 15 15 9.75M21 12a9 9 0 11-18 0 9 9 0 0118 0z" />
                                        </svg>
                                        </div>
                                        <span className="text-base text-gray-500 dark:text-white">Share and extension of knowledge</span>
                                    </li>
                                    <li className="flex items-center space-x-4 font-medium">
                                        <div>
                                        <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" strokeWidth="1.5" stroke="currentColor" aria-hidden="true" className="h-6 text-green-500">
                                            <path strokeLinecap="round" strokeLinejoin="round" d="M9 12.75L11.25 15 15 9.75M21 12a9 9 0 11-18 0 9 9 0 0118 0z" />
                                        </svg>
                                        </div>
                                        <span className="text-base text-gray-500 dark:text-white">Higher ranked and qualitative publications</span>
                                    </li>
                                    <li className="flex items-center space-x-4 font-medium">
                                        <div>
                                        <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" strokeWidth="1.5" stroke="currentColor" aria-hidden="true" className="h-6 text-green-500">
                                            <path strokeLinecap="round" strokeLinejoin="round" d="M9 12.75L11.25 15 15 9.75M21 12a9 9 0 11-18 0 9 9 0 0118 0z" />
                                        </svg>
                                        </div>
                                        <span className="text-base text-gray-500 dark:text-white">Increase of patents</span>
                                    </li>
                                    
                                </ul>
                            </div>
                            <div className="relative w-full md:w-auto" data-headlessui-state>
                                <span className="Button bg-yellow-500    cursor-pointer rounded-full" aria-expanded="false" data-headlessui-state id="headlessui-popover-button-:rm:">
                                    <span className="ButtonLarge flex items-center space-x-4">
                                        <span className="text-sm font-bold text-white">Subscribe</span>
                                            {/* <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor" aria-hidden="true" className="h-5 w-5 transition duration-150 ease-in-out group-hover:text-opacity-80">
                                                <path fillRule="evenodd" d="M5.23 7.21a.75.75 0 011.06.02L10 11.168l3.71-3.938a.75.75 0 111.08 1.04l-4.25 4.5a.75.75 0 01-1.08 0l-4.25-4.5a.75.75 0 01.02-1.06z" clipRule="evenodd" />
                                            </svg> */}
                                    </span>
                                </span>
                            </div>
                        </div>
                        
                        
                    </div>
                </div>
            </div>
        </div>
    </section>
   </React.Fragment>
  )
}
