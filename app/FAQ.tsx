import React from 'react'
import { ChevronDownIcon } from '@heroicons/react/24/outline'


export default function FAQ() {
  return (
   <React.Fragment>
    <section className="bg-white py-24 sm:py-32 lg:py-40">
        <div className="container mx-auto">
            <div className="-mx-4 flex flex-wrap">
                <div className="w-full px-4">
                    <div className="mx-auto mb-[60px] max-w-[520px] text-center lg:mb-20">
                    <span className="mb-2 block text-lg font-semibold text-yellow-600">
                        FAQ
                    </span>
                    <h2 className="mb-4 text-3xl font-bold text-black sm:text-4xl md:text-[40px]">
                        Any Questions? Look Here
                    </h2>
                    <p className="text-base text-body-color">
                        Here you'll find answers to some of the most common questions we receive about our products and services. 
                        We've organized our FAQ into categories to make it easier for you to fin    d the information you need.
                        Can't find the answer to your question here? No problem! Just contact us and we'll be happy to assist you.
                    </p>
                    </div>
                </div>
            </div>
            <div className="-mx-4 flex flex-wrap">
            <div className="w-full px-4 lg:w-1/2">
                <div className="single-faq mb-8 w-full rounded-lg border hover:border-[#ffd260] bg-white p-4 sm:p-8 lg:px-6 xl:px-8">
                <button className="faq-btn flex w-full text-left">
                    <div className="mr-5 flex h-10 w-full max-w-[40px] items-center justify-center rounded-lg bg-primary bg-opacity-5 text-primary">
                        <ChevronDownIcon className="h-8 w- text-[#ffd260]" />
                    </div>
                    <div className="w-full">
                    <h4 className="text-lg font-semibold text-black">
                        How do I use IROC ?
                    </h4>
                    </div>
                </button>
                <div x-show="openFaq1" className="faq-content pl-[62px]">
                    <p className="py-3 text-base leading-relaxed text-body-color">
                        Just contact us via email or Linkedin.
                        <br />
                            Email : <a href="mailto:contactus.iroc@gmail.com">IROC</a> or  <a href="mailto:nolwenn.morris95@gmail.com">Nolwenn Morris</a>
                    </p>
                </div>
                </div>

                <div className="single-faq mb-8 w-full rounded-lg border hover:border-[#ffd260] bg-white p-4 sm:p-8 lg:px-6 xl:px-8">
                <button className="faq-btn flex w-full text-left">
                    <div className="mr-5 flex h-10 w-full max-w-[40px] items-center justify-center rounded-lg bg-primary bg-opacity-5 text-primary">
                        <ChevronDownIcon className="h-8 w- text-[#ffd260]" />
                    </div>
                    <div className="w-full">
                    <h4 className="text-lg font-semibold text-black">
                        What does IROC Logbook tool offer compared to a laboratory notebook?
                    </h4>
                    </div>
                </button>
                <div x-show="openFaq3" className="faq-content pl-[62px]">
                    <p className="py-3 text-base leading-relaxed text-body-color">
                    IROC logbook tool allows you to take notes during your experiments, or in your daily life as a researcher. Your notes can be rephrased into sentences that you can send directly to your lab notebook in different sections (such as conditions, results, conclusions, discussions, ....). You can also request a live search of literature articles directly related to your results, allowing you to be very up-to-date with your literature. Finally through the search function, you can find all notes months and years later very easily compared to a physical notebook. The IROC logbook tool allows you to make the transition from your life as a researcher in the lab with notes to the production of analyzed and publishable results in your electronic lab notebook. 
                    </p>
                </div>
                </div>
            </div>
            <div className="w-full px-4 lg:w-1/2">
                <div className="single-faq mb-8 w-full rounded-lg border hover:border-[#ffd260] bg-white p-4 sm:p-8 lg:px-6 xl:px-8">
                        <button className="faq-btn flex w-full text-left">
                            <div className="mr-5 flex h-10 w-full max-w-[40px] items-center justify-center rounded-lg bg-primary bg-opacity-5 text-primary">
                                <ChevronDownIcon className="h-8 w- text-[#ffd260]" />
                            </div>
                            <div className="w-full">
                            <h4 className="text-lg font-semibold text-black">
                                What does IROC Literature review tool do for me compared to the databases and filters? 
                            </h4>
                            </div>
                        </button>
                        <div x-show="openFaq2" className="faq-content pl-[62px]">
                            <p className="py-3 text-base leading-relaxed text-body-color">
                            IROC Literature review tool uses the texts obtained from the databases and does your work of reading, sorting, then comparing and analyzing the information presented in these texts. Depending on the function you have chosen (there are 4 different ones), IROC will present you with a concentrate of information already preprocessed for you. All you have to do is read the information and use it for your own experiences.
                            </p>
                        </div>
                    </div>
            </div>
            </div>
        </div>
        <div className="absolute bottom-0 right-0 z-[-1]">
            <svg width={1440} height={886} viewBox="0 0 1440 886" fill="none" xmlns="http://www.w3.org/2000/svg">
            <path opacity="0.5" d="M193.307 -273.321L1480.87 1014.24L1121.85 1373.26C1121.85 1373.26 731.745 983.231 478.513 729.927C225.976 477.317 -165.714 85.6993 -165.714 85.6993L193.307 -273.321Z" fill="url(#paint0_linear)" />
            <defs>
                <linearGradient id="paint0_linear" x1="1308.65" y1="1142.58" x2="602.827" y2="-418.681" gradientUnits="userSpaceOnUse">
                <stop stopColor="#3056D3" stopOpacity="0.36" />
                <stop offset={1} stopColor="#F5F2FD" stopOpacity={0} />
                <stop offset={1} stopColor="#F5F2FD" stopOpacity="0.096144" />
                </linearGradient>
            </defs>
            </svg>
        </div>
    </section>
   </React.Fragment>
  )
}
