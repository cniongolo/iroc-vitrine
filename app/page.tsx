import React from 'react'
import Header from './Header'
import Features from './Features'
import CTA from './CTA'
import HeaderBanner from './banner'
import Footer from './Footer'
import IROC from './IROC'
import Pricing from './Pricing'
import FAQ from './FAQ'
import Logos from './Logos'

export default function Home() {
  return (
    <React.Fragment>
        <Header />
        <IROC  />
        <CTA />
        <Features />
        <Pricing />
        <Logos />
        <FAQ />
        <Footer />
    </React.Fragment>
  )
}
