"use client"
import React from 'react';
import dynamic from 'next/dynamic';

const navigation = [
  { 
    name: 'General', 
    href: '#',
    links : [
      { name: 'Who we are', href: '#' },
      { name: 'Applications', href: '#' },
      { name: 'Who we are', href: '#' },
    ]
  },
  { 
    name: 'Products', 
    href: '#',
    links : [
      { name: 'Documentation', href: '#' },
      { name: 'Help Center', href: '#' },
      { name: 'FAQ', href: '#' },
    ]
  },
  { 
    name: 'Legal', 
    href: '#',
    links : [
      { name: 'Terms of Service', href: '#' },
      { name: 'Privacy Policy', href: '#' },
      { name: 'Cookie Policy', href: '#' },
    ]
  },
];

export default function Footer() {
  return (
    <React.Fragment>
      <hr className='w-full border border-gray-50 dark:border-black-400' />
       <footer className="py-8 lg:py-24">
          <div className="container mx-auto px-5">
            <div className="flex flex-col space-y-8 lg:flex-row lg:space-y-0">
              <div className="flex w-full space-x-2 lg:w-4/12 xl:w-3/12 xl:space-x-6 2xl:space-x-8">
                <div className="flex flex-col space-y-4">
                  <div>
                    <img src="https://cdn.fs.brandfolder.com/YeH8YOVsQcCYC00c5fMw/convert?rotate=exif&fit=max&w=400&h=300&format=png" alt="logo" className='w-[70px] sm:w-[70px] w-[70px] md:w-[115px]' />
                  </div>
                  <div>
                    <p className='text-gray-500 dark:text-gray-500'>
                      International Research Optimization Center
                    </p>
                  </div>
                  <div className="flex items-center justify-between mt-12">
                    <div className="flex items-center order-2 space-x-6">
                      {/* <a href="https://twitter.com/BraydonCoyer" className="text-gray-600 dark:text-gray-400 important">
                          <span className="sr-only">Twitter</span>
                          <svg className="w-7 h-7 transform hover:rotate-[-4deg] transition" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <path d="M9.31 18.25C14.7819 18.25 17.7744 13.4403 17.7744 9.26994C17.7744 9.03682 17.9396 8.83015 18.152 8.73398C18.8803 8.40413 19.8249 7.49943 18.8494 5.97828C18.2031 6.32576 17.6719 6.51562 16.9603 6.74448C15.834 5.47393 13.9495 5.41269 12.7514 6.60761C11.9785 7.37819 11.651 8.52686 11.8907 9.62304C9.49851 9.49618 6.69788 7.73566 5.1875 5.76391C4.39814 7.20632 4.80107 9.05121 6.10822 9.97802C5.63461 9.96302 5.1716 9.82741 4.75807 9.58305V9.62304C4.75807 11.1255 5.75654 12.4191 7.1444 12.7166C6.70672 12.8435 6.24724 12.8622 5.80131 12.771C6.19128 14.0565 7.87974 15.4989 9.15272 15.5245C8.09887 16.4026 6.79761 16.8795 5.45806 16.8782C5.22126 16.8776 4.98504 16.8626 4.75 16.8326C6.11076 17.7588 7.69359 18.25 9.31 18.2475V18.25Z" stroke="currentColor" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"></path>
                          </svg>
                      </a> */}
                      <a href="https://www.linkedin.com/company/irocapplications/" className="text-gray-600 dark:text-gray-400 hover:text-yellow-500 important">
                          <span className="sr-only">LinkedIn</span>
                          <svg className="w-7 h-7 transform hover:rotate-[-4deg] transition" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <path d="M4.75 7.75C4.75 6.09315 6.09315 4.75 7.75 4.75H16.25C17.9069 4.75 19.25 6.09315 19.25 7.75V16.25C19.25 17.9069 17.9069 19.25 16.25 19.25H7.75C6.09315 19.25 4.75 17.9069 4.75 16.25V7.75Z" stroke="currentColor" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"></path>
                            <path d="M10.75 16.25V14C10.75 12.7574 11.7574 11.75 13 11.75C14.2426 11.75 15.25 12.7574 15.25 14V16.25" stroke="currentColor" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"></path>
                            <path d="M10.75 11.75V16.25" stroke="currentColor" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"></path>
                            <path d="M7.75 11.75V16.25" stroke="currentColor" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"></path>
                            <path d="M7.75 8.75V9.25" stroke="currentColor" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"></path>
                          </svg>
                      </a>
                    </div>
                  </div>
                  <div className='flex text-xs text-gray-500 dark:text-gray-500'>
                    <p>
                      © Copyright 2022 IROC. All Rights Reserved.
                    </p>
                    
                  </div>
                </div>
              </div>
              <div className='flex flex-col space-y-8 lg:space-y-0 lg:space-x-6 xl:space-x-16 2xl:space-x-20 w-full lg:flex-row lg:justify-end'>
                <div>
                  <div className='flex flex-col space-y-4'>
                    <h5 className='text-lg font-semibold'>
                      Our Company
                    </h5>
                    <ul className='flex flex-col space-y-4 text-gray-600 dark:text-gray-500'>
                      <li className='text-sm xl:text-base'>
                        <a href="/">Who we are</a>
                      </li>
                      <li className='text-sm xl:text-base'>
                        <a href="/">Applications</a>
                      </li>
                      <li className='text-sm xl:text-base'>
                        <a href="/">Contact</a>
                      </li>
                    </ul>
                  </div>
                </div>

                <div>
                  <div className='flex flex-col space-y-2.5'>
                    <h5 className='text-lg font-semibold'>
                      Products
                    </h5>
                    <ul className='flex flex-col space-y-4 text-gray-600 dark:text-gray-500'>
                      <li className='text-sm xl:text-base'>
                        <a href="/">Documentation</a>
                      </li>
                      <li className='text-sm xl:text-base'>
                        <a href="/">Help Center</a>
                      </li>
                      <li className='text-sm xl:text-base'>
                        <a href="/">Changelog</a>
                      </li>
                    </ul>
                  </div>
                </div>

                <div className='flex flex-col space-y-4'>
                    <h5 className='text-lg font-semibold'>
                        Legal
                    </h5>
                    <ul className='flex flex-col space-y-4 text-gray-600 dark:text-gray-500'>
                      <li className='text-sm xl:text-base'>
                        <a href="/">Terms of Service</a>
                      </li>
                      <li className='text-sm xl:text-base'>
                        <a href="/">Privacy Policy</a>
                      </li>
                      <li className='text-sm xl:text-base'>
                        <a href="/">Cookie Policy</a>
                      </li>
                    </ul>
                </div>

                <div className='flex flex-col space-y-4'>
                  <h5 className='text-lg font-semibold'>
                      Subscribe to our Newsletter
                      </h5>
                      <div className='text-sm text-gray-500 dark:text-gray-400 xl:text-base'>
                        Get the latest updates from our team.
                      </div>
                      <div>
                        <form action="" className='flex w-full flex-col justify-center space-y-2 lg:flex-row lg:space-y-0 lg:space-x-1.5'>
                          <div className='TextFieldInputContainer w-full 2xl:w-60'>
                            <input className="TextFieldInput flex-1 w-full 2xl:w-60" type="email" name="email_address" aria-label="Your email address" placeholder="your@email.com" required value="" />
                          </div>
                          <button className="Button ButtonPrimary">
                            <span className="ButtonNormal w-full flex text-white-500 w-full items-center">
                              <span className="flex w-full flex-1 items-center justify-center">
                                Subscribe
                              </span>
                            </span>
                          </button>
                        </form>
                      </div>
                </div>
              </div>
            </div>
          </div>
        </footer>
    </React.Fragment>
  )
}
